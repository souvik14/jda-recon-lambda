package com.morrisons.reconciliation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.postgresql.copy.CopyManager;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.morrisons.reconcilation.dto.ReconcilationDomainObject;
import com.morrisons.reconcilation.dto.ReconciliationType;
import com.morrisons.reconcilation.util.RDSUtil;
import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.config.ApplicationConstants;

public class JdaReconcilationDao extends BaseDao {

	private static final Logger logger = LogManager.getLogger(JdaReconcilationDao.class);
	private long lastTransactionTranData;
	private ApplicationConfig appconfig;
	private RDSUtil rdsUtil;
	private SessionFactory reconSessionFactory;

	@Inject
	public JdaReconcilationDao(ApplicationConfig appconfig, RDSUtil rdsUtil,
			@Named(RDSUtil.RECON_DB) SessionFactory reconSessionFactory) {
		this.appconfig = appconfig;
		this.rdsUtil = rdsUtil;
		this.reconSessionFactory = reconSessionFactory;
	}

	public List<ReconcilationDomainObject> fetchPoReceiptFromTranDataTable(String lastTransactionValue) {
		String sql = "SELECT container_id,reference_id,update_qty,sku_id,from_loc_id,to_loc_id,update_quantity_sign FROM wh_tran_data WHERE code IN ('Receipt','Receipt Reverse') AND transaction_id<="
				+ lastTransactionTranData + "AND transaction_id>=";
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preStatement = null;
		String finalQuery = sql + String.valueOf(Integer.valueOf(lastTransactionValue) + 1);
		logger.info("fetch query:: " + finalQuery);
		List<ReconcilationDomainObject> reconToBeInsertedList = new ArrayList<>();
		try {
			connection = rdsUtil.getRDSConnection(RDSUtil.JDA_DB);
			preStatement = connection.prepareStatement(finalQuery);
			resultSet = preStatement.executeQuery();
			resultSet.setFetchSize(appconfig.getFetchSize());

			while (resultSet.next()) {
				int updateQuantity = resultSet.getInt(3);
				if (resultSet.getString(7).equals("-")) {
					updateQuantity *= -1;
				}
				ReconcilationDomainObject reconObject = new ReconcilationDomainObject();
				reconObject.setContainerId(resultSet.getString(1));
				reconObject.setPurchaseOrderId(resultSet.getString(2));
				reconObject.setJdafacadeqty(updateQuantity);
				reconObject.setItem(resultSet.getString(4));
				reconObject.setSourcelocation(resultSet.getString(5));
				reconObject.setDestinationlocation(resultSet.getString(6));

				reconToBeInsertedList.add(reconObject);

			}

		} catch (Exception ex) {
			logger.error("Error retrieving PO receipt details from tran data table", ex);

		} finally {
			rdsUtil.closeConnection(connection, preStatement, resultSet);
		}
		logger.info("fetchPoReceiptFromTranDataTable :: Size list" + reconToBeInsertedList.size());
		return reconToBeInsertedList;
	}

	public void updateReconObject(List<ReconcilationDomainObject> reconData, ReconciliationType type) {
		StringBuilder query = new StringBuilder(
				"SELECT transactionid,jdafacadeqty FROM recon_extract_data WHERE transactiontype=:type AND item=:item AND sourcelocation=:source AND destinationlocation=:destination AND containerid=:containerid");
		for (ReconcilationDomainObject obj : reconData) {
			Map<String, Object> params = new HashMap<>();
			if (type.equals(ReconciliationType.PO_RECEIPT)) {
				query.append(" AND purchaseorderid=:purchaseorderid");
				params.put("type", ReconciliationType.PO_RECEIPT.getValue());
				params.put("purchaseorderid", obj.getPurchaseOrderId());

			} else if (type.equals(ReconciliationType.ASN)) {
				params.put("type", ReconciliationType.ASN.getValue());
			}

			params.put("item", obj.getItem());
			params.put("source", obj.getSourcelocation());
			params.put("destination", obj.getDestinationlocation());
			params.put("containerid", obj.getContainerId());
			@SuppressWarnings("unchecked")
			List<Object[]> record = (List<Object[]>) executeSelectSQLQuery(reconSessionFactory.openSession(),
					query.toString(), params);
			if (!record.isEmpty()) {
				obj.setInsert(false);
				obj.setTransactionID(Long.valueOf(String.valueOf(record.get(0)[0])));
				int value = Integer.parseInt(String.valueOf(record.get(0)[1]));
				obj.setJdafacadeqty(obj.getJdafacadeqty() + value);
			} else {
				obj.setInsert(true);
			}
		}
	}

	public void insertReconData(List<ReconcilationDomainObject> reconData, ReconciliationType type) {
		String sql = "COPY recon_extract_data (transactionid,transactiontype,purchaseorderid,asnid,containerid,item,rmsqty,jdafacadeqty,sourcelocation,destinationlocation,createdtimestamp,updatedtimestamp,reconcileflag) FROM STDIN WITH DELIMITER ','";
		Connection connection = null;
		StringBuilder builder = new StringBuilder(ApplicationConstants.EMPTY_STRING);
		int recordCount = 0;
		try {
			connection = rdsUtil.getRDSConnection(RDSUtil.RECON_DB);
			CopyManager cpm = getCopyManager(connection);

			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
				for (ReconcilationDomainObject obj : reconData) {
					Timestamp timestamp = new Timestamp(Calendar.getInstance().getTimeInMillis());
					builder.append(getNextValue(connection, ApplicationConstants.RECON_INDEX_NAME))
							.append(ApplicationConstants.COMMA).append(type.getValue())
							.append(ApplicationConstants.COMMA).append(obj.getPurchaseOrderId())
							.append(ApplicationConstants.COMMA).append(obj.getAsnId())
							.append(ApplicationConstants.COMMA).append(obj.getContainerId())
							.append(ApplicationConstants.COMMA).append(obj.getItem()).append(ApplicationConstants.COMMA)
							.append(obj.getRmsqty()).append(ApplicationConstants.COMMA).append(obj.getJdafacadeqty())
							.append(ApplicationConstants.COMMA).append(obj.getSourcelocation())
							.append(ApplicationConstants.COMMA).append(obj.getDestinationlocation())
							.append(ApplicationConstants.COMMA).append(timestamp).append(ApplicationConstants.COMMA)
							.append(timestamp).append(ApplicationConstants.COMMA).append(obj.getReconcileflag())
							.append(System.lineSeparator());

					if (++recordCount % appconfig.getBatchSize() == 0) {
						copyIn(cpm, sql, builder);
						builder = new StringBuilder(ApplicationConstants.EMPTY_STRING);
						recordCount = 0;
					}

				}
				copyIn(cpm, sql, builder);
				connection.commit();
				builder = null;
			}

		} catch (Exception ex) {
			logger.error("Error inserting data into Recon table", ex);
		} finally {
			rdsUtil.closeConnection(connection, null, null);
		}

	}

	public void updateReconData(List<ReconcilationDomainObject> reconData) {
		StringBuilder query = new StringBuilder(
				"UPDATE recon_extract_data SET jdafacadeqty=?,updatedtimestamp=? WHERE transactionid=?");
		int count = 0;
		Connection connection = null;
		try {
			connection = rdsUtil.getRDSConnection(RDSUtil.RECON_DB);
			connection.setAutoCommit(false);
			try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
				for (ReconcilationDomainObject obj : reconData) {
					preparedStatement.setInt(1, obj.getJdafacadeqty());
					preparedStatement.setTimestamp(2, new Timestamp(Calendar.getInstance().getTimeInMillis()));
					preparedStatement.setLong(3, obj.getTransactionID());

					preparedStatement.addBatch();
					if (++count % appconfig.getBatchSize() == 0) {
						preparedStatement.executeBatch();
						preparedStatement.clearParameters();
					}
				}
				preparedStatement.executeBatch();
				preparedStatement.clearParameters();
				connection.commit();
			}
		} catch (Exception ex) {
			logger.error("Error updating JDA quanity to recon table", ex);
		} finally {
			rdsUtil.closeConnection(connection, null, null);
		}

	}

	public List<ReconcilationDomainObject> fetchAsnFromTranDataTable(String lastTransactionValue) {
		String sql = "SELECT (SELECT whdecode FROM whsitedecode site WHERE site.jdasitename=dat.site_id AND site.jdasiteowner=dat.owner_id"
				+ "AND site.status='C') AS shippedFromLocationId,(SELECT REPLACE(UPPER(dat.customer_id),'CUS','')) AS destinationLocationId,"
				+ "(COALESCE(dat.pallet_id, dat.container_id,'loose')) AS containerId," + "dat.sku_id,"
				+ "dat.update_qty" + "FROM wh_tran_data dat WHERE UPPER(code)='SHIPMENT' AND transaction_id<="
				+ lastTransactionTranData + " AND transaction_id>=";
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement preStatement = null;
		String finalQuery = sql + String.valueOf(Integer.valueOf(lastTransactionValue) + 1);
		logger.info("fetch query :: " + finalQuery);
		List<ReconcilationDomainObject> asnList = new ArrayList<>();
		try {
			connection = rdsUtil.getRDSConnection(RDSUtil.JDA_DB);
			preStatement = connection.prepareStatement(finalQuery);
			resultSet = preStatement.executeQuery();
			resultSet.setFetchSize(appconfig.getFetchSize());

			while (resultSet.next()) {
				ReconcilationDomainObject reconObject = new ReconcilationDomainObject();
				reconObject.setSourcelocation(resultSet.getString(1));
				reconObject.setDestinationlocation(resultSet.getString(2));
				reconObject.setContainerId(resultSet.getString(3));
				reconObject.setItem(resultSet.getString(4));
				reconObject.setJdafacadeqty(resultSet.getInt(5));
				asnList.add(reconObject);
			}
		} catch (Exception ex) {
			logger.error(ex);

		} finally {
			rdsUtil.closeConnection(connection, preStatement, resultSet);
		}
		logger.info("Size list" + asnList.size());
		return asnList;
	}

	public long populateJdaTranDataLastTransactionId() {
		String sql = "SELECT MAX(transaction_id) FROM wh_tran_data";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = rdsUtil.getRDSConnection(RDSUtil.JDA_DB);
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				lastTransactionTranData = resultSet.getLong(1);
				return lastTransactionTranData;
			}

		} catch (Exception ex) {
			logger.error("Error fetching last transaction from wh_tran_data table", ex);
		} finally {
			rdsUtil.closeConnection(connection, statement, resultSet);
		}
		return -1;
	}

}
