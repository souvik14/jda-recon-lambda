package com.morrisons.reconciliation.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.postgresql.copy.CopyManager;
import org.postgresql.jdbc.PgConnection;

public class BaseDao {
	
	protected long getNextValue(Connection connection, String seqName) throws SQLException {
		String seqStr = "SELECT nextval( ? )";

		try (PreparedStatement preparedStatement = connection.prepareStatement(seqStr)) {
			
			preparedStatement.setString(1, seqName);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				if (resultSet.next()) {
					return resultSet.getLong(1);
				}
			}

		}
		return 0;
	}
	
	protected CopyManager getCopyManager(Connection con) throws SQLException {
        PgConnection copyOperationConnection = con.unwrap(PgConnection.class);
        return new CopyManager(copyOperationConnection);
    }
	
	protected void copyIn(CopyManager cpm, String sql, StringBuilder builder) throws SQLException, IOException {
        InputStream is = new ByteArrayInputStream(builder.toString().getBytes());
        cpm.copyIn(sql, is);
        is.close();
    }
	
	protected Object executeSelectSQLQuery(Session session, String query, Map<String, Object> map) {

		Transaction transaction = null;
		Object result = null;
		try {

			transaction = session.beginTransaction();
			Query sqlQuery = session.createSQLQuery(query).setProperties(map);
			result = sqlQuery.list();
			transaction.commit();

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw e;
		} finally {
			session.close();
		}
		return result;

	}

}
