package com.morrisons.reconciliation.module.provider;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Provider class for AWSCredentialsProvider
 */
public class WMMS3ClientProvider implements Provider<AmazonS3> {

    private final Regions region;
    private final ClientConfiguration clientConfiguration;
    private final AWSCredentialsProvider credentialsProvider;

    @Inject
    public WMMS3ClientProvider(Regions region, ClientConfiguration clientConfiguration, AWSCredentialsProvider credentialsProvider) {

        this.region = region;
        this.clientConfiguration = clientConfiguration;
        this.credentialsProvider = credentialsProvider;
    }

    /**
     * AWSStaticCredentialsProvider will be returned only for local development.
     * During execution on cloud DefaultAWSCredentialsProviderChain will always be
     * returned.
     */
    @Override
    public final AmazonS3 get() {

        return AmazonS3Client.builder()
                .withRegion(region)
                .withCredentials(credentialsProvider)
                .withClientConfiguration(clientConfiguration)
                .build();
    }
}