package com.morrisons.reconciliation.module.provider;

import com.amazonaws.auth.*;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.model.AWSConfig;

import org.apache.commons.lang3.StringUtils;

/**
 * Provider class for AWSCredentialsProvider
 */
public class AWSCredentialsProviderProvider implements Provider<AWSCredentialsProvider> {

    private final ApplicationConfig applicationConfig;

    @Inject
    public AWSCredentialsProviderProvider(ApplicationConfig applicationConfig) {

        this.applicationConfig = applicationConfig;
    }

    /**
     * AWSStaticCredentialsProvider will be returned only for local development.
     * During execution on cloud DefaultAWSCredentialsProviderChain will always be
     * returned.
     */
    @Override
    public final AWSCredentialsProvider get() {

        AWSConfig awsConfig = applicationConfig.getAwsConfig();

        if (StringUtils.isNotBlank(awsConfig.getAccessKey())) {
            AWSCredentials credentials = new BasicAWSCredentials(awsConfig.getAccessKey(), awsConfig.getSecretKey());
            return new AWSStaticCredentialsProvider(credentials);
        } else {
            return new DefaultAWSCredentialsProviderChain();
        }
    }
}