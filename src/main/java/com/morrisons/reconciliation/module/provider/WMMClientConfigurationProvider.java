package com.morrisons.reconciliation.module.provider;

import com.amazonaws.ClientConfiguration;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.model.ProxyConfig;

/**
 * Extending com.amazonaws.ClientConfiguration
 */

@Singleton
public class WMMClientConfigurationProvider extends ClientConfiguration {

    @Inject
    public WMMClientConfigurationProvider(ApplicationConfig applicationConfig) {

        super();

        ProxyConfig proxyConfig = applicationConfig.getProxyConfiguration();
        if (proxyConfig.getIsBehindProxy()) {
            withProxyHost(proxyConfig.getProxyHost()).withProxyPort(proxyConfig.getProxyPort())
                    .withProxyUsername(proxyConfig.getProxyUser())
                    .withProxyPassword(proxyConfig.getProxyPass());
        }
        withConnectionTimeout(30 * 60 * 1000)
                .withSocketTimeout(30 * 60 * 1000)
                .withMaxErrorRetry(9);
    }
}
