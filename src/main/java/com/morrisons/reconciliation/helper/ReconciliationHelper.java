package com.morrisons.reconciliation.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.inject.Inject;
import com.morrisons.reconcilation.dto.ReconcilationDomainObject;
import com.morrisons.reconcilation.dto.ReconciliationType;
import com.morrisons.reconciliation.config.ApplicationConstants;
import com.morrisons.reconciliation.dao.JdaReconcilationDao;

public class ReconciliationHelper {

	private JdaReconcilationDao reconDao;

	@Inject
	public ReconciliationHelper(JdaReconcilationDao reconDao) {
		this.reconDao = reconDao;
	}

	public List<ReconcilationDomainObject> fetchPoReceiptFromTranDataTable(String lastTranDataProcessed) {
		return updatedSourceReconObject(reconDao.fetchPoReceiptFromTranDataTable(lastTranDataProcessed));
	}

	public void updateReconObject(List<ReconcilationDomainObject> reconData, ReconciliationType type) {
		reconDao.updateReconObject(reconData, type);
	}

	public void insertUpdateReconData(List<ReconcilationDomainObject> reconData, ReconciliationType type) {
		List<ReconcilationDomainObject> insertReconList = reconData.stream().filter(ReconcilationDomainObject::isInsert)
				.collect(Collectors.toList());
		List<ReconcilationDomainObject> updateReconList = reconData.stream().filter(s -> !s.isInsert())
				.collect(Collectors.toList());
		if (!insertReconList.isEmpty()) {
			reconDao.insertReconData(insertReconList, type);
		}
		if (!updateReconList.isEmpty()) {
			reconDao.updateReconData(updateReconList);
		}

	}

	public List<ReconcilationDomainObject> fetchAsnFromTranDataTable(String lastTranDataProcessed) {
		return updatedSourceReconObject(reconDao.fetchAsnFromTranDataTable(lastTranDataProcessed));
	}

	public long populateJdaTranDataLastTransactionId() {
		return reconDao.populateJdaTranDataLastTransactionId();
	}

	private List<ReconcilationDomainObject> updatedSourceReconObject(
			List<ReconcilationDomainObject> sourceReconObject) {
		Map<String, ReconcilationDomainObject> map = new HashMap<>();
		if (!sourceReconObject.isEmpty()) {
			for (ReconcilationDomainObject obj : sourceReconObject) {
				String key = obj.getContainerId() + ApplicationConstants.UNDERSCORE + obj.getItem()
						+ ApplicationConstants.UNDERSCORE + obj.getSourcelocation() + ApplicationConstants.UNDERSCORE
						+ obj.getDestinationlocation();
				if (map.containsKey(key)) {
					ReconcilationDomainObject source = map.get(key);
					source.setJdafacadeqty(source.getJdafacadeqty() + obj.getJdafacadeqty());
					map.put(key, source);
				} else {
					map.put(key, obj);
				}
			}
		}
		return map.values().stream().collect(Collectors.toList());
	}

}
