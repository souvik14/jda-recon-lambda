package com.morrisons.reconciliation.config;

import com.morrisons.reconciliation.model.AWSConfig;
import com.morrisons.reconciliation.model.DatabaseConfig;
import com.morrisons.reconciliation.model.ProxyConfig;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationConfig {
	
	private AWSConfig awsConfig;
	private ProxyConfig proxyConfiguration;
	private DatabaseConfig jdaDatabaseConfig;
	private DatabaseConfig reconDatabaseConfig;
	private String getSelectQuery;
	private String insertToReconDomainTable;
	private String updatetoReconDomainTable;
	private String selectReconSelectTable;
	private Integer batchSize;
	private Integer fetchSize;

}
