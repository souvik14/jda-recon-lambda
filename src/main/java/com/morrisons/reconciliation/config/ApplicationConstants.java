package com.morrisons.reconciliation.config;

public class ApplicationConstants {
	
	private ApplicationConstants() {}
	
	public static final String EMPTY_STRING = "";
	public static final String COMMA = ",";
	public static final String UNDERSCORE = "_";
	public static final String RECON_INDEX_NAME = "recon_data_extract_sequence";

}
