package com.morrisons.reconciliation.config.module;

import org.hibernate.SessionFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.morrisons.reconcilation.util.RDSUtil;
import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.model.AWSConfig;
import com.morrisons.reconciliation.model.DatabaseConfig;
import com.morrisons.reconciliation.model.ProxyConfig;
import com.morrisons.reconciliation.module.provider.AWSCredentialsProviderProvider;
import com.morrisons.reconciliation.module.provider.WMMClientConfigurationProvider;
import com.morrisons.reconciliation.module.provider.WMMS3ClientProvider;

public class ApplicationModule extends AbstractModule {
	
	private final ApplicationConfig applicationConfig;
	private final SessionFactory jdaSessionFactory;
	private final SessionFactory reconSessionFactory;
	
	public ApplicationModule(ApplicationConfig applicationConfig, SessionFactory sessionFactory, SessionFactory reconSessionFactory) {
		this.applicationConfig = applicationConfig;
		this.jdaSessionFactory = sessionFactory;
		this.reconSessionFactory = reconSessionFactory;
	}
	
	@Override
	public void configure() {
		bindChannel();
		bindAwsComponents();
		bindDatabaseComponents();
	}

	private void bindDatabaseComponents() {
		bind(SessionFactory.class).annotatedWith(Names.named(RDSUtil.JDA_DB)).toInstance(jdaSessionFactory);
		bind(SessionFactory.class).annotatedWith(Names.named(RDSUtil.RECON_DB)).toInstance(reconSessionFactory);
	}

	private void bindAwsComponents() {
		bind(Regions.class).toInstance(Regions.valueOf(applicationConfig.getAwsConfig().getRegion()));
        bind(ClientConfiguration.class).to(WMMClientConfigurationProvider.class);
        bind(AWSCredentialsProvider.class).toProvider(AWSCredentialsProviderProvider.class).in(Singleton.class);
        bind(AmazonS3.class).toProvider(WMMS3ClientProvider.class).in(Singleton.class);		
	}

	private void bindChannel() {
		bind(ApplicationConfig.class).toInstance(applicationConfig);
		bind(AWSConfig.class).toInstance(applicationConfig.getAwsConfig());
		bind(DatabaseConfig.class).annotatedWith(Names.named(RDSUtil.JDA_DB)).toInstance(applicationConfig.getJdaDatabaseConfig());
		bind(DatabaseConfig.class).annotatedWith(Names.named(RDSUtil.RECON_DB)).toInstance(applicationConfig.getReconDatabaseConfig());
		bind(ProxyConfig.class).toInstance(applicationConfig.getProxyConfiguration());
	}

}
