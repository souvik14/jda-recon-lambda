package com.morrisons.reconciliation.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatabaseConfig {
	
	private String username;
	private String password;
	private String url;

}
