package com.morrisons.reconciliation.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AWSConfig {
	
	private String region;
	private String accessKey;
	private String secretKey;
	private String bucket;
	private String file;

}
