package com.morrisons.reconciliation.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProxyConfig {

	private Boolean isBehindProxy;
	private String proxyHost;
	private int proxyPort;
	private String proxyUser;
	private String proxyPass;
	
}
