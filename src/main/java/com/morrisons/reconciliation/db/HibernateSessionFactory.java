package com.morrisons.reconciliation.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.model.DatabaseConfig;

public class HibernateSessionFactory {

	private static final String HIBERNATE_CFG_FILE = "hibernate.cfg.xml";

	private HibernateSessionFactory() {
		// Added a private constructor to hide the implicit public one.
	}

	public static SessionFactory getJdaSessionfactory(ApplicationConfig applicationConfig) {
		DatabaseConfig databaseConfig = applicationConfig.getJdaDatabaseConfig();
		return generateSessionFactory(databaseConfig);
	}
	
	private static SessionFactory generateSessionFactory(DatabaseConfig databaseConfig) {
		try {

			Configuration configuration = new Configuration();
			configuration.configure();

			
			configuration.setProperty(AvailableSettings.USER, databaseConfig.getUsername());
			configuration.setProperty(AvailableSettings.PASS, databaseConfig.getPassword());
			configuration.setProperty(AvailableSettings.URL, databaseConfig.getUrl());

			StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).configure(HIBERNATE_CFG_FILE).build();

			Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();

			return metaData.getSessionFactoryBuilder().build();

		} catch (Exception e) {

			throw e;
		}
	}
	
	public static SessionFactory getReconSessionfactory(ApplicationConfig applicationConfig) {
		DatabaseConfig databaseConfig = applicationConfig.getReconDatabaseConfig();
		return generateSessionFactory(databaseConfig);
	}
}
