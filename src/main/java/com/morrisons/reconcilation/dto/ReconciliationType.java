package com.morrisons.reconcilation.dto;

public enum ReconciliationType {

	PO_RECEIPT("PO RECEIPT"), ASN("SHIPMENT");
	
	private String value;
	
	private ReconciliationType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}
