package com.morrisons.reconcilation.dto;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReconcilationDomainObject {
	
	
	private long transactionID;
	private ReconciliationType transactionType;
	private String purchaseOrderId;
	private String asnId;
	private String containerId;
	private String item;
	private int rmsqty;
	private int jdafacadeqty;
	private String sourcelocation;
	private String destinationlocation;
	private Timestamp createdtimestamp;
	private Timestamp updatedtimestamp;
	private String reconcileflag;
	
	private boolean insert;
	
}
