package com.morrisons.reconcilation.processor;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.morrisons.reconcilation.dto.ReconcilationDomainObject;
import com.morrisons.reconcilation.dto.ReconciliationType;
import com.morrisons.reconcilation.util.AWSFileUtil;
import com.morrisons.reconciliation.helper.ReconciliationHelper;

public class JdaReconcilationProcessor {

	private static final Logger logger = LogManager.getLogger(JdaReconcilationProcessor.class);

	private AWSFileUtil awsUtil;
	private ReconciliationHelper helper;

	@Inject
	public JdaReconcilationProcessor(AWSFileUtil awsUtil, ReconciliationHelper helper) {
		this.awsUtil = awsUtil;
		this.helper = helper;
	}

	public void processReconcilation() {
		
		long lastTranFromDb = helper.populateJdaTranDataLastTransactionId();

		// PO Receipt process starts
		logger.info("PO Receipt processing starts");
		List<ReconcilationDomainObject> poReceiptReconData = helper.fetchPoReceiptFromTranDataTable(awsUtil.getLastTranDataProcessed());
		helper.updateReconObject(poReceiptReconData,ReconciliationType.PO_RECEIPT);
		helper.insertUpdateReconData(poReceiptReconData,ReconciliationType.PO_RECEIPT);
		logger.info("PO Receipt processing ends");
		// PO Receipt process ends
		
		// ASN process start
		logger.info("ASN processing starts");
		List<ReconcilationDomainObject> asnReconData = helper.fetchAsnFromTranDataTable(awsUtil.getLastTranDataProcessed());
		helper.updateReconObject(asnReconData, ReconciliationType.ASN);
		helper.insertUpdateReconData(asnReconData, ReconciliationType.ASN);
		logger.info("ASN processing ends");
		// ASN process ends
		
		awsUtil.updateLastTranDataProcessed(lastTranFromDb);
	}

}
