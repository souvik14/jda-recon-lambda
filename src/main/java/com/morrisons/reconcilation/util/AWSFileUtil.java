package com.morrisons.reconcilation.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.morrisons.reconciliation.model.AWSConfig;

@Singleton
public class AWSFileUtil {

	private static final String PO_RECEIPT_TRANSACTION_ID = "PO_RECEIPT_TRANSACTION_ID";
	private static final String ASN_TRANSACTION_ID = "ASN_TRANSACTION_ID";
	private static final Logger logger = LogManager.getLogger(AWSFileUtil.class);

	private AWSConfig awsConfig;
	private AmazonS3 amazonS3;
	private Map<String, String> configMap = new HashMap<>();

	@Inject
	public AWSFileUtil(AWSConfig awsConfig, AmazonS3 amazonS3) {
		this.amazonS3 = amazonS3;
		this.awsConfig = awsConfig;
		generateConfigFile();
	}

	private void generateConfigFile() {
		try (S3Object s3Object = amazonS3.getObject(new GetObjectRequest(awsConfig.getBucket(), awsConfig.getFile()));
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(s3Object.getObjectContent()))) {

			String line;
			while ((line = bufferedReader.readLine()) != null) {
				String[] split = line.split("=");
				configMap.put(split[0], split[1]);

			}
		} catch (Exception e) {
			logger.error("Error retrieving properties file from S3", e);
		}
	}

	public String getLastTranDataProcessed() {
		return configMap.get(PO_RECEIPT_TRANSACTION_ID);
	}
	
	public String getLastTranPubProcessed() {
		return configMap.get(ASN_TRANSACTION_ID);
	}

	public void updateLastTranDataProcessed(long lastTranFromDb) {
		configMap.put(PO_RECEIPT_TRANSACTION_ID, String.valueOf(lastTranFromDb));
		String properties = configMap.keySet().stream().map(key -> key + "=" + configMap.get(key)).collect(Collectors.joining(System.lineSeparator()));
		amazonS3.putObject(awsConfig.getBucket(), awsConfig.getFile(), properties);
	}

}
