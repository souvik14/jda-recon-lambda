package com.morrisons.reconcilation.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.morrisons.reconciliation.model.DatabaseConfig;

@Singleton
public class RDSUtil {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RDSUtil.class);
	public static final String JDA_DB = "jda_db";
	public static final String RECON_DB = "recon_db";
	private DatabaseConfig jdaDb;
	private DatabaseConfig reconDb;

	@Inject
	public RDSUtil(@Named(JDA_DB) DatabaseConfig jdaDb, @Named(RECON_DB) DatabaseConfig reconDb) {
		this.jdaDb = jdaDb;
		this.reconDb = reconDb;
	}

	public Connection getRDSConnection(String dbType) {
		DatabaseConfig config = null;
		Connection con = null;
		if (dbType.equals(JDA_DB)) {
			config = jdaDb;
		} else if (dbType.equals(RECON_DB)) {
			config = reconDb;
		}
		try {

			String hostname = config.getUrl();
			String userName = config.getUsername();
			String password = config.getPassword();

			String jdbcUrl = hostname + "?user=" + userName + "&password=" + password;
			logger.info("JDBC hostname : {}", hostname);
			con = DriverManager.getConnection(jdbcUrl);
		} catch (Exception e) {
			logger.error("Error connecting to DB", e);
		}

		return con;
	}

	public void closeConnection(Connection conn, PreparedStatement preStatement, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e) {

			}
		if (preStatement != null)
			try {
				preStatement.close();
			} catch (SQLException e) {

			}
		if (conn != null)
			try {
				conn.close();
			} catch (SQLException e) {

			}
	}

}
