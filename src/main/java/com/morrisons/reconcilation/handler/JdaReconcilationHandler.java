package com.morrisons.reconcilation.handler;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.morrisons.reconcilation.processor.JdaReconcilationProcessor;
import com.morrisons.reconciliation.config.ApplicationConfig;
import com.morrisons.reconciliation.config.ConfigLoader;
import com.morrisons.reconciliation.config.module.ApplicationModule;
import com.morrisons.reconciliation.db.HibernateSessionFactory;


public class JdaReconcilationHandler implements RequestHandler {
	
	private static Logger logger = LoggerFactory.getLogger(JdaReconcilationHandler.class);
	
	
	public static void main(String agrs[]){
		
		JdaReconcilationHandler handler = new JdaReconcilationHandler();
		System.out.println(handler.handleRequest(null, null));
	}

	
	public Object handleRequest(Object input, Context context) {
		ApplicationConfig appConfig = new ConfigLoader().getConfig();
		
		SessionFactory jdaSessionFactory = HibernateSessionFactory.getJdaSessionfactory(appConfig);
		SessionFactory reconSessionFactory = HibernateSessionFactory.getReconSessionfactory(appConfig);

		// Create Injector
		Injector injector = Guice
				.createInjector(new ApplicationModule(appConfig, jdaSessionFactory, reconSessionFactory));
		
		JdaReconcilationProcessor processor = injector.getInstance(JdaReconcilationProcessor.class);
		processor.processReconcilation();
		
		return "Success";
	}

}
